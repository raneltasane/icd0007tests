<?php

require_once('common.php');

const BASE_URL = 'http://localhost:8080';

class Hw3Tests extends HwTests {

    function baseUrlResponds() {
        $this->get(BASE_URL);
        $this->assertResponse(200);
    }

    function listPageHasMenuWithCorrectLinks() {
        $this->get(BASE_URL);

        $this->assertLinkById('list-page-link');
        $this->assertLinkById('add-page-link');

    }

    function addPageHasCorrectElementsAndIds() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->assertField('firstName');
        $this->assertField('lastName');
        $this->assertField('phone');

        $this->assertField('submitButton');
    }

    function applicationLinksShouldBeInCorrectFormat() {

        $this->get(BASE_URL);

        // expected format is ?key1=value1&key2=value2&...

        $this->assertFrontControllerLink('list-page-link');
        $this->assertFrontControllerLink('add-page-link');
    }

}

(new Hw3Tests())->run(new PointsReporter());
